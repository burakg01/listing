package com.listing.listingservice.contract.request;

public class DecreaseStockRequest {
    private int quantity;
    private int version;

    public DecreaseStockRequest(int quantity, int version) {
        this.quantity = quantity;
        this.version = version;
    }

    public DecreaseStockRequest() {
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
