package com.listing.listingservice.contract.response;

public class VersionResponse {
    private int version;

    public VersionResponse() {
    }

    public VersionResponse(int version) {
        this.version = version;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}